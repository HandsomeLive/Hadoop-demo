package top.cmagic.flume.interceptors;

import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.util.List;

/**
 * @author wsc
 * @date: 2020年02月21日 16:15
 * @since JDK 1.8
 */
public class TypeInterceptor implements Interceptor
{
    public void initialize() {

    }

    public Event intercept(Event event) {
        return null;
    }

    public List<Event> intercept(List<Event> list) {
        return null;
    }

    public void close() {

    }
}
