package top.cmagic.flume.source;

import org.apache.flume.Context;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.PollableSource;
import org.apache.flume.conf.Configurable;
import org.apache.flume.source.AbstractSource;

/**
 * @author wsc
 * @date: 2020年02月22日 18:22
 * @since JDK 1.8
 */
public class TypeSource extends AbstractSource implements Configurable, PollableSource {
    public Status process() throws EventDeliveryException {
        return null;
    }

    public long getBackOffSleepIncrement() {
        return 0;
    }

    public long getMaxBackOffSleepInterval() {
        return 0;
    }

    public void configure(Context context) {

    }
}
