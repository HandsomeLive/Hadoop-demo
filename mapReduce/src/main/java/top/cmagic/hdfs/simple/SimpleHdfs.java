package top.cmagic.hdfs.simple;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;

/**
 * @author wsc
 * @date: 2019年12月24日 17:10
 * @since JDK 1.8
 */
public class SimpleHdfs {

    private FileSystem fs;

    @Before
    public void before() throws IOException, InterruptedException {
        Configuration configuration = new Configuration();
        configuration.set("dfs.replication","3");
        fs = FileSystem.get(URI.create("hdfs://172.17.4.128:9000"), configuration,"centos");
    }

    @After
    public void after() throws IOException {
        fs.close();
        System.out.println("成功!!!");
    }

    @Test
    public void upload() throws IOException {
        fs.copyFromLocalFile(new Path("D:\\input\\zhangsan.txt"),new Path("/user/local/zhangsan.txt"));
    }

    @Test
    public void download() throws IOException {
        fs.copyToLocalFile(new Path("/user/local/zhangsan.txt"),new Path("D:\\input\\lisi.txt"));
    }
}
