package top.cmagic.mr.partition;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author wangsc
 * @date: 2020年01月11日 21:08
 * @since JDK 1.8
 */
public class FlowBeanPartitioner extends Partitioner<Text,FlowBean> {
    @Override
    public int getPartition(Text text, FlowBean flowBean, int i) {
        String prefix = text.toString().substring(0, 3);
        if ("136".equals(prefix)){
            return 0;
        }else if ("137".equals(prefix)){
            return 1;
        }else if ("138".equals(prefix)){
            return 2;
        }else if ("139".equals(prefix)){
            return 3;
        }else {
            return 4;
        }
    }
}
