package top.cmagic.mr.partition;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author wangsc
 * @date: 2020年01月11日 21:05
 * @since JDK 1.8
 */
public class PartitionerReducer extends Reducer<Text, FlowBean, Text, FlowBean> {
    @Override
    protected void reduce(Text key, Iterable<FlowBean> values, Context context) throws IOException, InterruptedException {
        for (FlowBean value : values) {

            context.write(key,value);
        }
    }
}
