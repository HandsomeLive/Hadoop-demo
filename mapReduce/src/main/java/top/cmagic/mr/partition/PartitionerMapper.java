package top.cmagic.mr.partition;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author wangsc
 * @date: 2020年01月11日 21:04
 * @since JDK 1.8
 */
public class PartitionerMapper extends Mapper<LongWritable, Text, Text, FlowBean> {

    private Text text = new Text();
    private FlowBean flowBean = new FlowBean();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split("\t");
        text.set(split[1]);
        flowBean.setBean(Long.parseLong(split[split.length - 3]), Long.parseLong(split[split.length - 2]));
        context.write(text, flowBean);
    }
}
