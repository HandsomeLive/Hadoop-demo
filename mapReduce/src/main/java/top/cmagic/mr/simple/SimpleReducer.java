/**
 * 
 */
package top.cmagic.mr.simple;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author wangsc
 * @date 2019年12月24日 下午12:33:40
 */
public class SimpleReducer extends Reducer<Text, IntWritable, Text, IntWritable>{

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapreduce.Reducer#reduce(java.lang.Object, java.lang.Iterable, org.apache.hadoop.mapreduce.Reducer.Context)
	 */
	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Context context) throws IOException, InterruptedException {
		int count=0;
		for (IntWritable intWritable : values) {
			int i = intWritable.get();
			count+=i;
		}
		context.write(key, new IntWritable(count));
	}
}
