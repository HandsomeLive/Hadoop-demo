/**
 * 
 */
package top.cmagic.mr.simple;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


/**
 * @author wangsc
 * @date 2019年12月24日 上午11:47:42
 */
public class SimpleMapper extends Mapper<LongWritable, Text, Text, IntWritable>{

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapreduce.Mapper#map(java.lang.Object, java.lang.Object, org.apache.hadoop.mapreduce.Mapper.Context)
	 */
	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String string = value.toString();
		String[] split = string.split(" ");
		for (String word : split) {
			context.write(new Text(word), new IntWritable(1));
		}
	}

}
